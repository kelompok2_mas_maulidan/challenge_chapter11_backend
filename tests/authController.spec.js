const request = require('supertest');
const app = require('../app');

describe('POST /api/register', () => {
    let url = '/api/register';
    
    test('request header is null', done => {
        let req = {};

        request(app).post(url).send(req)
        .then(res => {
            expect(res.statusCode).toBe(500);
            done();
        });
    });

    test('username is null', done => {
        let req = {
            'username': null
        };

        request(app).post(url).send(req)
        .then(res => {
            expect(res.statusCode).toBe(400);
            done();
        });
    });

    test('request header is fill', done => {
        let req = {
            'username': 'test',
            'password': 'test123',
            'email': 'test@test.com',
            'bio': 'bio test',
            'city': 'city test',
            'social_media_url': 'socmed test'
        }

        request(app).post(url).send(req)
        .then(res => {
            expect(res.statusCode).toBe(200);
            done();
        });
    });
});

describe('POST /api/login', () => {
    let url = '/api/login';

    test('email or password is null', done => {
        let req = {
            'email': null,
            'password': null
        }

        request(app).post(url).send(req)
        .then(res => {
            expect(res.statusCode).toBe(400);
            done();
        });
    });
});

describe('GET /api/users', () => {
    let url = '/api/users';

    test('get is success and response status code is 200', done => {
        request(app).get(url)
        .then(res => {
            expect(res.statusCode).toBe(200);
            done();
        });
    });
});