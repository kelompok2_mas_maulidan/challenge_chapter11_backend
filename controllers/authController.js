const { User } = require('../models');
const passport = require('../passport');

function format(user) {
    const { id, email } = user;

    return {
        id,
        email,
        accessToken: user.generateToken()
    }
}

module.exports = {
    getUsers: async (req, res) => {
        try {
            const users = await User.findAll({
                attributes: ['id', 'username', 'email', 'total_score', 'bio', 'city', 'social_media_url']
            });

            res.status(200).json({
                status: 'success',
                message: 'Berhasil get users',
                data: users
            });
        } catch (error) {
            res.status(500).json({
                status: 'error',
                message: error.message
            });
        }
    },
    getUserById: async (req, res) => {
        const id = req.params.id;

        try {
            const user = await User.findByPk(id);

            res.json(user);
        } catch (error) {
            console.log(error.message);
        }
    },
    updateUser: async (req, res) => {
        const id = req.params.id;

        User.updateUser(id, req.body)
        .then(response => {
            res.status(200).json({
                'status': 'success',
                'message': 'Berhasil update profile',
                'data': response
            });
        })
        .catch(err => {
            res.status(500).json({
                'status': 'error',
                'message': err
            });
        })
    },
    registerApi: (req, res) => {
        // Validation
        if (req.body.username === null) {
            return res.status(400).json({
                'status': 'error',
                'message': 'Username is required'
            });
        };

        User.register(req.body)
        .then(user => {
            res.status(200).json({
                'status': 'success',
                'message': 'Register berhasil, silahkan login',
                'data': user
            });
        })
        .catch((err) => {
            res.status(500).json({
                'status': 'error',
                'message': err,
            });
        });
    },
    loginApi: (req, res) => {
        // Validation
        if (req.body.email === null || req.body.password === null) {
            return res.status(400).json({
                'status': 'error',
                'message': 'Email or Password is required'
            });
        }

        User.authenticate(req.body)
        .then(user => {
            res.status(200).json({
                'status': 'success',
                'message': 'Anda berhasil login',
                'data': format(user)
            });
        })
        .catch(err => {
            res.status(500).json({
                'status': 'error',
                'message': err
            });
        });
    },
    logoutApi: async (req, res, next) => {
        req.logout(function (err) {
            if (err) return next(err);
            res.json({
                'status': 'success',
                'message': 'Anda berhasil logout'
            }, 200);
        });
    },
    addScore: async (req, res) => {
        // console.log(req);
        const id = req.params.id;

        User.addScore(id, req.body)
        .then(response => {
            res.status(200).json({
                'status': 'success',
                'message': 'Berhasil update score',
                'data': response
            });
        })
        .catch(err => {
            res.status(500).json({
                'status': 'error',
                'message': err
            });
        })
    }
}