'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = async ({ username, password, email, bio, city, social_media_url }) => {
      const encryptedPassword = this.#encrypt(password);

      try {
        const create = await this.create({ username, password: encryptedPassword, email, bio, city, social_media_url });
        
        if (!create) return Promise.reject('Periksa kembali data register anda.');

        return Promise.resolve(create);
      } catch (error) {
        
        return Promise.reject(error);
      }
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({ where: { email }});
        if (!user) return Promise.reject("Data yang anda masukan tidak sesuai.");

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Data yang anda masukan tidak sesuai.");

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    }

    static updateUser = async (id, { username, password, email, bio, city, social_media_url }) => {
      const encryptedPassword = this.#encrypt(password);
      const data = { 
        username: username,
        password: encryptedPassword,
        email: email,
        bio: bio,
        city: city,
        social_media_url, social_media_url
      };

      try {
        const update = await this.update(data, {
          where: { id: id }
        });

        if (!update) return Promise.reject('Gagal update profile.');

        return Promise.resolve(data);
      } catch (error) {
        console.log(error);
        return Promise.reject(error);
      }
    }

    static addScore = async (id, { score }) => {
      const user = await this.findOne({ where: { id }});

      let total_score;
      if (user.total_score == null) {
        total_score = 0 + parseInt(score);
      } else {
        total_score = parseInt(user.total_score) + parseInt(score);
      }

      try {
        const update = await this.update({ 
          total_score: total_score
        }, {
          where: { id: id }
        });

        if (!update) return Promise.reject('Gagal update score.');

        return Promise.resolve(update);
      } catch (error) {
        console.log(error);
        return Promise.reject(error);
      }
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        email: this.email
      }

      const secret = 'mastergame12345';
      const token = jwt.sign(payload, secret, { expiresIn: '1d' });

      return token;
    }

  }

  User.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    total_score: DataTypes.INTEGER,
    bio: DataTypes.STRING,
    city: DataTypes.STRING,
    social_media_url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};